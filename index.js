require("custom-env").env();
const express = require("express");
const mysql = require("mysql2");

const app = express();
app.use(express.json());

const { Host, DBUserName, Password, Database, port } = process.env;
const dbConfig = {
  host: Host,
  user: DBUserName,
  password: Password,
  database: Database,
};

const pool = mysql.createPool(dbConfig);

app.get("/", (req, res) => {
  pool.query("SELECT * FROM slots", (err, rows) => {
    if (err) {
      res.send("Error while fetching slot");
    } else {
      res.send(rows);
    }
  });
});

app.post("/", (req, res) => {
  const { date, startSlot, endSlot } = req.body;

  const startingTime = new Date(`${date} ${startSlot}`);
  const endingTime = new Date(`${date} ${endSlot}`);
  const GivenDate = new Date(date);

  pool.query(
    "SELECT * FROM slots WHERE date = ? AND (start_time < ? AND end_time > ?)", // Veryfying overlaps are there are not
    [GivenDate, endingTime, startingTime],
    (err, rows) => {
      if (err) {
        res.send("db connection error");
      } else if (rows.length > 0) {
        res.send("Slot already booked. please look for another slot");
      } else {
        if (startingTime.getTime() < endingTime.getTime()) {
          pool.query(
            "INSERT INTO slots (date, start_time, end_time) VALUES (?, ?, ?)",
            [GivenDate, startingTime, endingTime],
            (error) => {
              if (error) {
                res.send("Error while inserting data");
              } else {
                res.send("Slot created successfully");
              }
            }
          );
        } else {
          res.send("Start time should be less than end time");
        }
      }
    }
  );
});

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
